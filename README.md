# CRAVER-PUBLIC-ISSUES

Welcome! Please report any issues with the app here.

The latest binaries can always be found here:

https://apps.apple.com/app/lbcraver/id6446771297 (iOS / iPadOS)

https://play.google.com/store/apps/details?id=ch.cern.lbdaq.Craver&hl=en (Google)
